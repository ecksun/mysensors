PROJECTS := border-router button temperature

test_targets = $(addsuffix .test, $(PROJECTS))
$(test_targets): arduino-1.8.0
	make -C "$(basename $@)"

.PHONY: test $(test_targets)
test: $(test_targets)

arduino-1.8.0: build/arduino-1.8.0-linux64.tar.xz
	tar -xf build/arduino-1.8.0-linux64.tar.xz
	touch "$@"

build:
	mkdir -p build

build/arduino-1.8.0-linux64.tar.xz: build
	wget https://downloads.arduino.cc/arduino-1.8.0-linux64.tar.xz --output-document "$@"
	touch "$@"

clean_targets = $(addsuffix .clean, $(PROJECTS))
$(clean_targets):
	make -C "$(basename $@)" clean

.PHONY: clean-projects $(clean_targets)
clean-projects: $(clean_targets)

.PHONY: clean
clean: clean-projects
	rm -rf build arduino-1.8.0

#ifdef MY_OWN_DEBUG
#define debug_print(...) Serial.print(__VA_ARGS__)
#define debug_println(...) Serial.println(__VA_ARGS__)
#else
#define debug_print(...)
#define debug_println(...)
#endif

#define MY_RADIO_NRF24

#include <MySensors.h>

#define CHILD_ID 0

MyMessage msg(CHILD_ID, V_TEMP);

void setup() {
    Serial.begin(115200);
}

void presentation() {
  sendSketchInfo("TempSens", "0.2");
  present(CHILD_ID, S_TEMP);
}

int last_read = -1;

void loop() {
    const int raw_temperature = analogRead(A0);
    if (last_read != raw_temperature) {
        last_read = raw_temperature;

        const double voltage = raw_temperature / 1024.0 * 5;
        const double temperature = (voltage / 10 * 1000);

        debug_print("Analog read: ");
        debug_println(raw_temperature);
        debug_print("Voltage (V): ");
        debug_println(voltage);
        debug_print("Temperature (C):");
        debug_println(temperature);

        send(msg.set(temperature, 2));
    }

    sleep(10 * 1000);
}

#define MY_DEBUG

#define MY_RADIO_NRF24

#include <SPI.h>
#include <MySensors.h>
#include <Bounce2.h>

const int button_pin = 3;
Bounce debouncer = Bounce();

#define CHILD_ID 0
MyMessage msg(CHILD_ID, V_TRIPPED);

void setup()
{
  pinMode(button_pin, INPUT);
  digitalWrite(button_pin, HIGH);

  debouncer.attach(button_pin);
  debouncer.interval(5);
}

void presentation() {
  sendSketchInfo("Butt on", "0.1");
  present(CHILD_ID, S_DOOR);
}

int last_value = -1;

void loop()
{
  debouncer.update();

  int value = debouncer.read();

  if (value != last_value) {
     send(msg.set(value == HIGH ? 1 : 0));
     last_value = value;
  }
}


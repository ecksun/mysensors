# Dependencies

- arduino-mk

# Tips

To be able to upload with root, add yourself to the dialout group:

    adduser "$USER" dialout

# Debugging

To enable debugging we can set `CPPFLAGS`:

    export CPPFLAGS=-DMY_OWN_DEBUG -DMY_DEBUG

`MY_OWN_DEBUG` will enable debugging for the sketch itself, not the entire
MySensors codebase, which is what `MY_DEBUG` does.
